#%%
# Dependencies
print("\nLoading Modules")
from itertools import combinations, chain
import random
from collections import Counter
import networkx as nx
import matplotlib.pyplot as plt
from graphviz import Digraph

#%%
# Auxiliary Functions
print("Defining functions")


def C_Product(A, B):
    return {(a, b) for a in A for b in B}


def Power_Set(iterable):
    s = list(iterable)
    ps = chain.from_iterable(combinations(s, r) for r in range(len(s) + 1))
    return {frozenset(s) for s in ps}


#%%
# Definitions
print("Establishing definitions")
n = 10
U = set(range(1, n + 1))
N = {"X", "Y"}
UxN = C_Product(U, N)
PUxN = Power_Set(UxN)

# The power set of UxN contains elements for which either X, Y or their
# respective complements, as classes of U, are empty. This engender degenerate
# cases, in particular, making A (Every X is Y) not to be included in (i.e. not
# to imply) I (Some X is Y). De Morgan carefully excludes this cases and
# sometimes mentions this exclusion explicitly. Therefore, it is legitimate to
# construct a 'normalized' version of PUxN, called PUxN_Norm, and witten P(UxN)*

PUxN_Norm = {r for r in PUxN if {c[1] for c in r} == {c[1] for c in UxN - r} == N}


#%%
# Classical Propositional forms (from syllogistics). syllogistic_rel is a
# constructor, taking quantity (True = 'all',False = 'any') and quality (True =
# 'o in', False = 'o not in') parameters and resulting providing a
# characteristic function for the four classical propositional forms (A, E, I,
# O) over a set of relations between names and objects (elements of PUxN_Norm)

def syllogistic_rel(quantity:bool,quality:bool):
    def RQ(R):
        X = [u for u,n in R if n == "X"]
        Y = [u for u,n in R if n == "Y"]
        if quantity==True:
            if quality == True:
                rq = all([o in Y for o in X])
            else:
                rq = all([o not in Y for o in X])
        else:
            if quality == True:
                rq = any([o in Y for o in X])
            else:
                rq = any([o not in Y for o in X])
        return rq
    return RQ

A = syllogistic_rel(quantity = True, quality = True)
E = syllogistic_rel(quantity = True, quality = False)
I = syllogistic_rel(quantity = False, quality = True)
O = syllogistic_rel(quantity = False, quality = False)


#%%
# Simple Propositions A, E, I, O, - the 'd' in Ad, Ed, Id, Od stands for 'down'
# to refer to De Morgan's subaccents (eg: A,). Propositions are classes of
# relations R in PUxN* meeting the conditions defined above.

print("\nConstructing propositions")

print("A,: Every X is Y")
Ad = {R for R in PUxN_Norm if A(R)}
print("E,: No X is Y")
Ed = {R for R in PUxN_Norm if E(R)}
print("I,: Some X is Y")
Id = {R for R in PUxN_Norm if I(R)}
print("O,: Some X is not Y")
Od = {R for R in PUxN_Norm if O(R)}


#%%
# Contranominals (A' E' I' O' - the 'u' in Au, Eu, Iu, Ou stands for 'up').
# Contranominals (notated with superaccents in De Morgan, eg: A') can be
# interpreted through complementarity of R over UxN (ie. UxN - R in the code)
print("\nConstructing contranominals")

print("A': Every ¬X is ¬Y")
Au = {R for R in PUxN_Norm if A(UxN - R)}
print("E': No ¬X is ¬Y")
Eu = {R for R in PUxN_Norm if E(UxN - R)}
print("I': Some ¬X is ¬Y")
Iu = {R for R in PUxN_Norm if I(UxN - R)}
print("O': Some ¬X is not ¬Y")
Ou = {R for R in PUxN_Norm if O(UxN - R)}
# %%
# Complex Propositions. Intersection in P(UxN)* adequately interprets the
# 'coexistance' defining complex propositions (which De Morgan notates with a
# '+' but also refers to with 'and')
print("\nConstructing Complex Propositions")

print("D,: Subidentical")
Dd = set.intersection(Ad,Ou)
print("D : Identical")
D = set.intersection(Ad,Au)
print("D': Superidentical")
Du = set.intersection(Au,Od)

print("C,: Subcontrary")
Cd = set.intersection(Ed,Iu)
print("C : Contrary")
C = set.intersection(Ed,Eu)
print("C': Supercontrary")
Cu = set.intersection(Eu,Id)

print("P : Particular")
P = set.intersection(Id,Iu,Od,Ou)
# %%

# Test of correctness of dual relations between simple and complex propositions
# stated by De Morgan. And and Or are thus shown to be interpretable in terms of
# intersection and union in P(UxN)*

print("\nTesting correctness of (dual) relations between propositions")
# Affirmations
print("Affirmations:")
print(f"D, = A, and O': {Dd == Ad.intersection(Ou)}")
print(f"D  = A, and A': {D == Ad.intersection(Au)}")
print(f"D' = A' and O,: {Du == Au.intersection(Od)}")

print(f"C, = E, and I': {Cd == Ed.intersection(Iu)}")
print(f"C  = E, and E': {C == Ed.intersection(Eu)}")
print(f"C' = E' and I,: {Cu == Eu.intersection(Id)}")


print(f"A, = D, or D: {Ad == Dd.union(D)}")
print(f"A' = D' or D: {Au == Du.union(D)}")
print(f"E, = C, or C: {Ed == Cd.union(C)}")
print(f"E' = C, or C: {Eu == Cu.union(C)}")

# Denials (Denials are shown to be interpretable in terms of complementarity in
# P(UxN)*)
print("Denials:")
print(
    f"¬D, = ¬(A, and O') = ¬A, or ¬O' = O, or A': {PUxN_Norm - Dd == PUxN_Norm - Ad.intersection(Ou) == (PUxN_Norm - Ad).union(PUxN_Norm - Ou) == Od.union(Au)}"
)

print(
    f"¬D  = ¬(A, and A') = ¬A, or ¬A' = O, or O': {PUxN_Norm - D == PUxN_Norm - Ad.intersection(Au) == (PUxN_Norm - Ad).union(PUxN_Norm - Au) == Od.union(Ou)}"
)

print(
    f"¬D' = ¬(A' and O,) = ¬A' or ¬O, = O' or A,: {PUxN_Norm - Du == PUxN_Norm - Au.intersection(Od) == (PUxN_Norm - Au).union(PUxN_Norm - Od) == Ou.union(Ad)}"
)

print(
    f"¬C, = ¬(E, and I') = ¬E, or ¬I' = I, or E': {PUxN_Norm - Cd == PUxN_Norm - Ed.intersection(Iu) == (PUxN_Norm - Ed).union(PUxN_Norm - Iu) == Id.union(Eu)}"
)

print(
    f"¬C  = ¬(E, and E') = ¬E, or ¬E' = I, or I': {PUxN_Norm - C == PUxN_Norm - Ed.intersection(Eu) == (PUxN_Norm - Ed).union(PUxN_Norm - Eu) == Id.union(Iu)}"
)

print(
    f"¬C' = ¬(E' and I,) = ¬E' or ¬I, = I' or E,: {PUxN_Norm - Cu == PUxN_Norm - Eu.intersection(Id) == (PUxN_Norm - Eu).union(PUxN_Norm - Id) == Iu.union(Ed)}"
)

print(
    f"¬A, = O, = ¬D, and ¬D: {(PUxN_Norm - Ad) == Od == (PUxN_Norm - Dd).intersection(PUxN_Norm - D)}"
)
print(
    f"¬A' = O' = ¬D' and ¬D: {(PUxN_Norm - Au) == Ou == (PUxN_Norm - Du).intersection(PUxN_Norm - D)}"
)
print(
    f"¬E, = I, = ¬C, and ¬C: {(PUxN_Norm - Ed) == Id == (PUxN_Norm - Cd).intersection(PUxN_Norm - C)}"
)
print(
    f"¬E' = I' = ¬C' and ¬C: {(PUxN_Norm - Eu) == Iu == (PUxN_Norm - Cu).intersection(PUxN_Norm - C)}"
)

# %%
# Test determining the truth of propositions given a particular X and Y in U
# (through a particular R constructed by randomly choosing a sample of elements
# of UxN. Size of the sample: between 2 and n-2 elements)

print("\nTest of a particular R in P(UxN)\n")
Prop_Simple = ["Ad", "Au", "Ed", "Eu", "Id", "Iu", "Od", "Ou"]
Prop_Complex = ["Dd", "D", "Du", "Cd", "C", "Cu", "P"]
Propositions = Prop_Simple + Prop_Complex + ["PUxN_Norm", "{}"]
R = set(random.sample(UxN, random.randint(2, n - 2)))

print(f"R = {sorted(list(R))}\n")

if R not in PUxN_Norm:
    print("R is degenerate (not in the normalized P(UxN))")
else:
    Result_Simple = [p for p in Prop_Simple if R in list(eval(p))]
    Result_Complex = [p for p in Prop_Complex if R in list(eval(p))]

    print(f"True simple (or 'disjunctive') propositions: {Result_Simple}")
    print(f"True complex (or 'conjunctive') propositions: {Result_Complex}")


# %%

# Build a graph representing the inclusion between propositions
print("\nConstructing lattice of propositions")
Prop_posets = nx.DiGraph()

for i in Propositions:
    for j in Propositions:
        if set(eval(j)).issubset(set(eval(i))) and i != j:
            Prop_posets.add_edge(
                i.replace("d", ",").replace("u", "’").replace("PUxN_Norm", "P(UxN)*"),
                j.replace("d", ",").replace("u", "’"),
            )

# Compute the transitive reduction of the graph
Prop_posets_tred = nx.transitive_reduction(Prop_posets)
# %%
Prop_graph = Digraph(name="De Morgan Lattice")
for e, f in Prop_posets_tred.edges():
    Prop_graph.edge(e, f)

for node in Prop_Simple:
    Prop_graph.node(
        node.replace("d", ",").replace("u", "’"), style="filled", color="thistle"
    )

for node in Prop_Complex:
    Prop_graph.node(
        node.replace("d", ",").replace("u", "’"), style="filled", color="lightsteelblue"
    )

for node in ["PUxN_Norm", "{}"]:
    Prop_graph.node(node.replace("PUxN_Norm", "P(UxN)*"), style="filled", color="gray")

Prop_graph.view()
print("Hesse diagram of lattice of propositions printed")
print("Done!")
# %%
